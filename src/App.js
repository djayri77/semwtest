import React, { Component } from 'react';
import './App.css';
import Denominations from './components/Denominations';
import InputText from './components/input/InputText';
import ErrorNotification from './components/notification/ErrorNotification';

class App extends Component {
  state = {
    availableFractions: [ 100000, 50000, 20000, 10000, 5000, 2000, 1000, 500, 100, 50 ],
    denominations: [],
    errorMessage:''
  };
  isInputValid = strNominal => {
    /** check if all char allowed */
    if(!this.isAllCharAllowed(strNominal)) return {valid:false,message:'invalid input char'};
    /** check if currency valid */
    if(!this.isCurrencyValid(strNominal)) return {valid:false,message:'invalid currency'};
    /** check if comma valid */
    if(!this.isDecimalValid(strNominal)) return {valid:false, message:'invalid decimal value'};
    /** check if separator valid and placed correctly */
    if(!this.isSeparatorValid(strNominal)) return {valid:false, message:'invalid separator'};

    strNominal = this.getNominalValue(strNominal);
    if(!strNominal.length) return {valid:false, message:'invalid input'};

    return {valid: true, nominal:parseInt(strNominal)};
  }

  getFractions = (nominal) => {
    let denominations = [];
    for(var i=0; i<this.state.availableFractions.length; i++){
      const fraction = this.state.availableFractions[i]; 
      if(nominal >= fraction){
        const fracTotal = Math.floor(nominal/fraction);
        denominations = [...denominations,{frac:fraction, total:fracTotal}];
        nominal -= fraction*fracTotal;
      } else if(nominal<Math.min(...this.state.availableFractions)) break;
    }
    if(nominal>0) denominations.push({frac: 'left', nominal:nominal});
    return denominations;
  }

  getDenominations = (event) => {
    if(event.keyCode==13){
      const validate = this.isInputValid(event.target.value);
      if(validate.valid){
        this.setState({errorMessage:''});
        this.setState({ denominations: this.getFractions(validate.nominal) });
      } else this.setState({denominations:[],errorMessage:validate.message});
    }
  }

  getNominalValue = strNominal => {
    strNominal = this.removeCurrency(strNominal);
    strNominal = strNominal.replace(/\./g,'');
    const behindComma = strNominal.split(',');
    strNominal = behindComma[0];
    return strNominal;
  }

  isAllCharAllowed = strNominal => {
    strNominal = strNominal.replace(/\./g,''); // replace all dots
    strNominal = this.removeCurrency(strNominal);
    strNominal = strNominal.replace(',',''); // comma
    strNominal = strNominal.replace(/[0-9]/g,''); // remove all numeric value
    return strNominal.length==0; // all left currency and commas will return as invalid char
  }

  isCurrencyValid = strNominal => {
    const indexCurrency = strNominal.indexOf('Rp');
    if(indexCurrency>-1) 
      return indexCurrency==0;
    return true;
  }

  isDecimalValid = strNominal => {
    const countComma = strNominal.split(','),
          decimalValue = countComma[countComma.length-1];
    if(strNominal.indexOf(',')>-1)
      return decimalValue=='00';
    return true;
  }
  
  isSeparatorValid = strNominal => {
    strNominal = this.removeCurrency(strNominal);
    const splitComma = strNominal.split(','); 
    strNominal = splitComma[0]; // ge only value before comma
    if(strNominal.indexOf('.')>-1){
      const splitVal = strNominal.split('.');
      const firstVal = splitVal.shift();
      if(firstVal.length==0 || firstVal.length>3) return false;
      for(var i=0;i<splitVal.length;i++)
        if(splitVal[i].length!=3) return false;
    }
    return true;
  }

  removeCurrency = strNominal => {
    strNominal = strNominal.replace('Rp','');
    if(strNominal.substring(0,1)==' ') strNominal = strNominal.substring(1);
    return strNominal;
  }

  render() {
    return (
      <div className="App">
        <div className="app-title">Get Denominations App</div>
        <InputText keydown={this.getDenominations}></InputText>
        <ErrorNotification message={this.state.errorMessage}></ErrorNotification>
        <Denominations denominations={this.state.denominations}></Denominations>
      </div>
    );
  }
}

export default App;
