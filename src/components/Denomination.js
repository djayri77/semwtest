import React from 'react';

const Denomination = (props) => (
    <div className="denomination">
        <p>
            <span className="fraction-total">{props.total}</span>
            <span className="fraction-value">x Rp{props.denomination}</span>
        </p>
    </div>
);

export default Denomination;