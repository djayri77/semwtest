import React, {Component} from 'react';
import Denomination from './Denomination';

class Denominations extends Component{
   // shouldComponentUpdate(nexProps)
    render(){
        const denominationElem = this.props.denominations.map((denomination) => {
            return denomination.frac=='left' ? (<p key={denomination.frac}>Left: {denomination.nominal} no available fraction.</p>):
            <Denomination key={denomination.frac} denomination={denomination.frac} total={denomination.total}></Denomination>
        });
        return (
            <div className="denominations">{denominationElem}</div>
        );
    }
}

export default Denominations;
