import React from 'react';

const ErrorNotification = props => {
    return props.message.length ? (
        <div className="error-notification">
            <p>{props.message}</p>
        </div>
    ):('');
}

export default ErrorNotification;
