import React from 'react';

const inputText = (props) => (
    <div className="input-text">
        <label className="label-input">Please insert nominal in Rp</label>
        <input placeholder="input nominal" type="text" className="input-rupiah" name={props.name} value={props.value} onKeyDown={(event)=>{props.keydown(event)}}></input>
    </div>
);

export default inputText;