# semwtest

this is Software Engineer Mobile Web Test by Muh Djayusman Riyadhi

Please visit http://157.230.33.224 (hosted in DigitalOcean.com) to run this app.


How to install and run this Project on your local machine or your own server.
1. This projects is created with ReactJS framework, includes all generated code by create-react-app.
2. please clone this repositories to your local computer/server.
3. if you use local computer, just open terminal, install Node.JS and navigate to this project directory root, and start Node server by type "npm start" and your local computer will open new browser tab.
4. if you host this project to linux server, do the following:
    4.1. install Node.JS .
    4.2. setup NGINX.
    4.3. setup NGINX block server, and point the root directory to this project_root/build.
    4.4. restart NGINX.
    4.5. and open the browser then type project url as defined in NGINX block server before.

thanks for the opportunity.





